package com.fsm.app;

import android.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManagerListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Kevin on 10/12/14.
 */
public class OpeningActivity extends Activity {

	private String imgPath;

	private DeckOfCardsManager deckOfCardsManager;
	private RemoteResourceStore remoteResourceStore;
	private TextView latitudeText;
	private TextView longitudeText;
	private ListCard fsmCards;

	private String nameOfCardSent;

	private LocationManager locationManager;
	private LocationListener locationListener;

	private final double latitudeCoordinate = 37.86965;
	private final double longitudeCoordinate = -122.25914;
	@Override

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opening);

		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		latitudeText = (TextView)findViewById(R.id.latitude_text);
		longitudeText = (TextView)findViewById(R.id.longitude_text);

		locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {

				double latitude = location.getLatitude();
				double longitude = location.getLongitude();
//

				latitudeText.setText("Latitude: " + latitude);
				longitudeText.setText("Longitude: " + longitude);

				Location sproul = new Location("sproul");
				sproul.setLatitude(latitudeCoordinate);
				sproul.setLongitude(longitudeCoordinate);

				if (deckOfCardsManager != null && sproul.distanceTo(location) <= 50) {
					if (deckOfCardsManager.isConnected()) {
						String[] notificationStringArray = new String[6];
						notificationStringArray[0] = "Open the card named Jack Weinberg, and then open the FSM app on your phone.";
						notificationStringArray[1] = "Open the card named Joan Baez, and then open the FSM app on your phone.";
						notificationStringArray[2] = "Open the card named Michael Rossman, and then open the FSM app on your phone.";
						notificationStringArray[3] = "Open the card named Art Goldberg, and then open the FSM app on your phone.";
						notificationStringArray[4] = "Open the card named Jackie Goldberg, and then open the FSM app on your phone.";
						notificationStringArray[5] = "Open the card named Mario Savio, and then open the FSM app on your phone.";

						int randomIndex = new Random().nextInt(notificationStringArray.length);

						if (randomIndex == 0) {
							nameOfCardSent = "Jack Weinberg";
						} else if (randomIndex == 1) {
							nameOfCardSent = "Joan Baez";
						} else if (randomIndex == 2) {
							nameOfCardSent = "Michael Rossman";
						} else if (randomIndex == 3) {
							nameOfCardSent = "Art Goldberg";
						} else if (randomIndex == 4) {
							nameOfCardSent = "Jackie Goldberg";
						} else if (randomIndex == 5) {
							nameOfCardSent = "Mario Savio";
						}

//						nameOfCardSent = "Mario Savio";

						RemoteToqNotification notification = new RemoteToqNotification(getApplicationContext(), 100000, "Draw Request!", new String[]{notificationStringArray[randomIndex]});

						try {
							deckOfCardsManager.sendNotification(notification);

//							locationManager.removeUpdates(locationListener);

						} catch (RemoteDeckOfCardsException e) {
							e.printStackTrace();
						}

					}
				}
			}
			public void onStatusChanged(String provider, int status, Bundle extras) {}
			public void onProviderEnabled(String provider) {}
			public void onProviderDisabled(String provider) {}
		};


		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 0, locationListener);
		}
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 0, locationListener);
		}

		deckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
		remoteResourceStore = new RemoteResourceStore();
		fsmCards = new ListCard();

		SimpleTextCard card0 = new SimpleTextCard("Mario Savio");
		card0.setHeaderText("Mario Savio");
		Bitmap imageBitmap0 = BitmapFactory.decodeResource(getResources(), R.drawable.mario_savio_toq);
		Bitmap scaledBitmap0 = Bitmap.createScaledBitmap(imageBitmap0, 250, 288, false);
		CardImage cardImage0 = new CardImage("Mario Savio", scaledBitmap0);
		remoteResourceStore.addResource(cardImage0);

		card0.setCardImage(remoteResourceStore, cardImage0);
		card0.setMessageText(new String[]{"Express your own view of free speech in an image"});
		card0.setShowDivider(true);
		card0.setReceivingEvents(true);

		SimpleTextCard card1 = new SimpleTextCard("Jack Weinberg");
		card1.setHeaderText("Jack Weinberg");
		Bitmap imageBitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.jack_weinberg_toq);
		Bitmap scaledBitmap1 = Bitmap.createScaledBitmap(imageBitmap1, 250, 288, false);
		CardImage cardImage1 = new CardImage("Jack Weinberg", scaledBitmap1);
		remoteResourceStore.addResource(cardImage1);

		card1.setCardImage(remoteResourceStore, cardImage1);
		card1.setMessageText(new String[]{"Draw Text: FSM"});
		card1.setShowDivider(true);
		card1.setReceivingEvents(true);

		SimpleTextCard card2 = new SimpleTextCard("Joan Baez");
		card2.setHeaderText("Joan Baez");
		Bitmap imageBitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.joan_baez_toq);
		Bitmap scaledBitmap2 = Bitmap.createScaledBitmap(imageBitmap2, 250, 288, false);
		CardImage cardImage2 = new CardImage("Joan Baez", scaledBitmap2);
		remoteResourceStore.addResource(cardImage2);

		card2.setCardImage(remoteResourceStore, cardImage2);
		card2.setMessageText(new String[]{"Draw Image of: A Megaphone"});
		card2.setShowDivider(true);
		card2.setReceivingEvents(true);

		SimpleTextCard card3 = new SimpleTextCard("Jackie Goldberg");
		card3.setHeaderText("Jackie Goldberg");
		Bitmap imageBitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.jackie_goldberg_toq);
		Bitmap scaledBitmap3 = Bitmap.createScaledBitmap(imageBitmap3, 250, 288, false);
		CardImage cardImage3 = new CardImage("Jackie Goldberg", scaledBitmap3);
		remoteResourceStore.addResource(cardImage3);

		card3.setCardImage(remoteResourceStore, cardImage3);
		card3.setMessageText(new String[]{"Draw Text: SLATE"});
		card3.setShowDivider(true);
		card3.setReceivingEvents(true);

		SimpleTextCard card4 = new SimpleTextCard("Michael Rossman");
		card4.setHeaderText("Michael Rossman");
		Bitmap imageBitmap4 = BitmapFactory.decodeResource(getResources(), R.drawable.michael_rossman_toq);
		Bitmap scaledBitmap4 = Bitmap.createScaledBitmap(imageBitmap4, 250, 288, false);
		CardImage cardImage4 = new CardImage("Michael Rossman", scaledBitmap4);
		remoteResourceStore.addResource(cardImage4);

		card4.setCardImage(remoteResourceStore, cardImage4);
		card4.setMessageText(new String[]{"Draw Text: Free Speech"});
		card4.setShowDivider(true);
		card4.setReceivingEvents(true);

		SimpleTextCard card5 = new SimpleTextCard("Art Goldberg");
		card5.setHeaderText("Art Goldberg");
		Bitmap imageBitmap5 = BitmapFactory.decodeResource(getResources(), R.drawable.art_goldberg_toq);
		Bitmap scaledBitmap5 = Bitmap.createScaledBitmap(imageBitmap5, 250, 288, false);
		CardImage cardImage5 = new CardImage("Art Goldberg", scaledBitmap5);
		remoteResourceStore.addResource(cardImage5);

		card5.setCardImage(remoteResourceStore, cardImage5);
		card5.setMessageText(new String[]{"Draw Text: Now"});
		card5.setShowDivider(true);
		card5.setReceivingEvents(true);

		fsmCards.add(card0);
		fsmCards.add(card1);
		fsmCards.add(card2);
		fsmCards.add(card3);
		fsmCards.add(card4);
		fsmCards.add(card5);


		Intent intent = getIntent();

		System.out.println(intent.toString());

		if (intent.getExtras() != null) {

			if (intent.getExtras().containsKey("flickImagePath")) {

				if (intent.getStringExtra("flickImagePath").length() > 0) {
					imgPath = intent.getStringExtra("flickImagePath");
				}

			}

			if (intent.getExtras().containsKey("submit_complete")) {

				if (intent.getStringExtra("submit_complete").length() > 0){
					showImage();
					Toast.makeText(getApplicationContext(), "Post submitted successfully, check your watch.", Toast.LENGTH_SHORT).show();
					System.out.println("Submit complete");
				}

			}


		}



	}


	@Override
	protected void onStart() {
		super.onStart();


		DeckOfCardsManagerListener deckOfCardsManagerListener = new DeckOfCardsManagerListener() {
			@Override
			public void onConnected() {
				try {
					if (deckOfCardsManager.isInstalled()) {
						deckOfCardsManager.updateDeckOfCards(new RemoteDeckOfCards(getApplicationContext(), fsmCards), remoteResourceStore);
					} else {
						deckOfCardsManager.installDeckOfCards(new RemoteDeckOfCards(getApplicationContext(), fsmCards), remoteResourceStore);
					}
				} catch (RemoteDeckOfCardsException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void onDisconnected() {
			}
			@Override
			public void onInstallationSuccessful() {
				Toast.makeText(getApplicationContext(), "toq installed succesfully!", Toast.LENGTH_SHORT).show();
			}
			@Override
			public void onInstallationDenied() {
			}
			@Override
			public void onUninstalled() {
				Toast.makeText(getApplicationContext(), "toq uninstalled succesfully", Toast.LENGTH_SHORT).show();
			}
		};

		DeckOfCardsEventListener deckOfCardsEventListener = new DeckOfCardsEventListener() {
			@Override
			public void onCardOpen(String s) {
				System.out.println("s : " + s);
				System.out.println("nameOfCardSent: " + nameOfCardSent);
				if (s.equals(nameOfCardSent)) {
					System.out.println("equals!");
					Intent mainIntent = new Intent(OpeningActivity.this, MainActivity.class);
					OpeningActivity.this.startActivity(mainIntent);
				}
			}
			@Override
			public void onCardVisible(String s) {
			}
			@Override
			public void onCardInvisible(String s) {
			}
			@Override
			public void onCardClosed(String s) {
			}
			@Override
			public void onMenuOptionSelected(String s, String s2) {
			}
			@Override
			public void onMenuOptionSelected(String s, String s2, String s3) {
			}
		};
		try {
			deckOfCardsManager.addDeckOfCardsManagerListener(deckOfCardsManagerListener);
			deckOfCardsManager.addDeckOfCardsEventListener(deckOfCardsEventListener);
			deckOfCardsManager.connect();
		} catch (RemoteDeckOfCardsException e) {
			e.printStackTrace();
		}


	}

	private void addCard(Bitmap bm) {
		SimpleTextCard card = new SimpleTextCard("New Card");
		card.setHeaderText("See what other people posted...");
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, 250, 288, false);
		CardImage cardImage = new CardImage("New Card", scaledBitmap);
		remoteResourceStore.addResource(cardImage);

		card.setCardImage(remoteResourceStore, cardImage);
		card.setMessageText(new String[]{"tags: cs160fsm"});
		card.setShowDivider(true);
		card.setReceivingEvents(true);
		fsmCards.add(card);

		try {
			deckOfCardsManager.updateDeckOfCards(new RemoteDeckOfCards(getApplicationContext(), fsmCards), remoteResourceStore);
		} catch (RemoteDeckOfCardsException e) {
			e.printStackTrace();
			Toast.makeText(this, "Failed to Create SimpleTextCard", Toast.LENGTH_SHORT).show();
		}

	}

	private void showImage() {
		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					String svr="www.flickr.com";

					REST rest=new REST();
					rest.setHost(svr);

					//initialize Flickr object with key and rest
					Flickr flickr=new Flickr(FlickrHelper.API_KEY,rest);

					//initialize SearchParameter object, this object stores the search keyword
					SearchParameters searchParams=new SearchParameters();
					searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);

					//Create tag keyword array
					String[] tags=new String[]{"cs160_fall2014", "cs160fsm"};
					searchParams.setTags(tags);

					//Initialize PhotosInterface object
					PhotosInterface photosInterface=flickr.getPhotosInterface();
					//Execute search with entered tags
					PhotoList photoList=photosInterface.search(searchParams,20,1);

					//get search result and fetch the photo object and get small square imag's url
					if(photoList!=null){
						//Get search result and check the size of photo result
						Random random = new Random();
						int seed = random.nextInt(photoList.size());
						//get photo object
						Photo photo=(Photo)photoList.get(seed);

						//Get small square url photo
						InputStream is = photo.getMediumAsStream();
						final Bitmap bm = BitmapFactory.decodeStream(is);

						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								System.out.println("run");
								addCard(bm);
								Intent intent = new Intent(getApplicationContext(), FlickrjActivity.class);
								intent.putExtra("flickImagePath", imgPath);
								startActivity(intent);
							}
						});
					}
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (FlickrException e) {
					e.printStackTrace();
				} catch (IOException e ) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};

		thread.start();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.location, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
