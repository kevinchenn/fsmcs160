package com.fsm.app;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.UUID;
import android.provider.MediaStore;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;


import org.json.JSONException;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	private DrawingView mDrawingView;

	private ImageButton mCurrentPaint;
	private ImageButton mBrushButton;
	private ImageButton mEraseButton;

	private Button mSubmitButton;

	private float smallBrushSize;
	private float mediumBrushSize;
	private float largeBrushSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		this.mDrawingView = (DrawingView)findViewById(R.id.drawing);
		this.mDrawingView.setBrushSize(mediumBrushSize);
		LinearLayout paletteLayout = (LinearLayout)findViewById(R.id.colors);

		this.mCurrentPaint = (ImageButton)paletteLayout.getChildAt(0);
		this.mCurrentPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));

		// Instantiate Brush Size
		this.smallBrushSize = getResources().getInteger(R.integer.small_size);
		this.mediumBrushSize = getResources().getInteger(R.integer.medium_size);
		this.largeBrushSize = getResources().getInteger(R.integer.large_size);

		this.mBrushButton = (ImageButton)findViewById(R.id.draw_button);
		this.mBrushButton.setOnClickListener(this);

		this.mEraseButton = (ImageButton)findViewById(R.id.erase_button);
		this.mEraseButton.setOnClickListener(this);

		this.mSubmitButton = (Button)findViewById(R.id.submit_button);
		this.mSubmitButton.setOnClickListener(this);

//		showImage();

	}

	@Override
	public void onClick(View view){
		if(view.getId() == R.id.draw_button) {

			final Dialog brushDialog = new Dialog(this);
			brushDialog.setTitle("Select brush size:");
			brushDialog.setContentView(R.layout.brush_dialog);

			ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setBrushSize(smallBrushSize);
					mDrawingView.setLastBrushSize(smallBrushSize);
					mDrawingView.setErase(false);
					brushDialog.dismiss();
				}
			});

			ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setBrushSize(mediumBrushSize);
					mDrawingView.setLastBrushSize(mediumBrushSize);
					mDrawingView.setErase(false);
					brushDialog.dismiss();
				}
			});

			ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setBrushSize(largeBrushSize);
					mDrawingView.setLastBrushSize(largeBrushSize);
					mDrawingView.setErase(false);
					brushDialog.dismiss();
				}
			});

			brushDialog.show();
		} else if(view.getId() == R.id.erase_button){
			final Dialog eraserDialog = new Dialog(this);
			eraserDialog.setTitle("Select eraser size:");
			eraserDialog.setContentView(R.layout.brush_dialog);

			ImageButton smallBtn = (ImageButton)eraserDialog.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setErase(true);
					mDrawingView.setBrushSize(smallBrushSize);
					eraserDialog.dismiss();
				}
			});
			ImageButton mediumBtn = (ImageButton)eraserDialog.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setErase(true);
					mDrawingView.setBrushSize(mediumBrushSize);
					eraserDialog.dismiss();
				}
			});
			ImageButton largeBtn = (ImageButton)eraserDialog.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					mDrawingView.setErase(true);
					mDrawingView.setBrushSize(largeBrushSize);
					eraserDialog.dismiss();
				}
			});

			eraserDialog.show();
		} else if (view.getId() == R.id.submit_button) {

			// Save Image from DrawingView
			Bitmap bm = mDrawingView.getDrawingCache();
			File fPath = Environment.getExternalStorageDirectory();
			File f = null;
			f = new File(fPath, "fsm_picture.png");

			try {
				FileOutputStream strm = new FileOutputStream(f);
				bm.compress(Bitmap.CompressFormat.PNG, 80, strm);
				strm.close();
			}
			catch (IOException e){
				e.printStackTrace();
			}


			// Upload picture to Flickr API
//			Intent intent = new Intent(getApplicationContext(), FlickrjActivity.class);
//			intent.putExtra("flickImagePath", f.getAbsolutePath());
//			startActivity(intent);

			Intent addCardIntent = new Intent(getApplicationContext(), OpeningActivity.class);
			addCardIntent.putExtra("submit_complete", "submit_complete");
			addCardIntent.putExtra("flickImagePath", f.getAbsolutePath());
			startActivity(addCardIntent);

		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	public void paintClicked(View view){
		if (view != this.mCurrentPaint) {
			mDrawingView.setErase(false);
			mDrawingView.setBrushSize(mDrawingView.getLastBrushSize());
			ImageButton imgView = (ImageButton) view;
			String color = view.getTag().toString();
			this.mDrawingView.setColor(color);

			imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
			this.mCurrentPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint));
			this.mCurrentPaint = (ImageButton)view;
		}
	}


}
