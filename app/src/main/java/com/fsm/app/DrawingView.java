package com.fsm.app;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.TypedValue;

/**
 * Created by Kevin on 10/11/14.
 */

public class DrawingView extends View {

	private Path mPath;

	private Paint mPaint;
	private Paint mCanvasPaint;

	final int initPaintColor = 0xFF660000;

	private Canvas mCanvas;

	private Bitmap mBitmap;

	private float mBrushSize;
	private float mLastBrushSize;

	private boolean isErasing = false;

	public DrawingView(Context context, AttributeSet attrs){
		super(context, attrs);
		setDrawingCacheEnabled(true);
		initDrawing();
	}

	private void initDrawing() {

		this.mPath = new Path();
		this.mPaint = new Paint();

		this.mPaint.setColor(initPaintColor);

		this.mPaint.setAntiAlias(true);
		this.mPaint.setStyle(Paint.Style.STROKE);
		this.mPaint.setStrokeJoin(Paint.Join.ROUND);

		this.mPaint.setStrokeCap(Paint.Cap.ROUND);

		this.mCanvasPaint = new Paint(Paint.DITHER_FLAG);

		this.mBrushSize = getResources().getInteger(R.integer.medium_size);
		this.mLastBrushSize = this.mBrushSize;

		this.mPaint.setStrokeWidth(this.mBrushSize);

	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		this.mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		this.mCanvas = new Canvas(this.mBitmap);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(this.mBitmap, 0 , 0, this.mCanvasPaint);
		canvas.drawPath(this.mPath, this.mPaint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float touchX = event.getX();
		float touchY = event.getY();

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				this.mPath.moveTo(touchX, touchY);
				break;
			case MotionEvent.ACTION_MOVE:
				this.mPath.lineTo(touchX, touchY);
				break;
			case MotionEvent.ACTION_UP:
				this.mCanvas.drawPath(this.mPath, this.mPaint);
				this.mPath.reset();
				break;
			default:
				return false;
		}

		invalidate();
		return true;
	}

	public void setColor(String newColor){
		invalidate();

		int paintColor = Color.parseColor(newColor);
		this.mPaint.setColor(paintColor);

	}

	public void setBrushSize(float newSize){
		float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				newSize, getResources().getDisplayMetrics());
		this.mBrushSize = pixelAmount;
		this.mPaint.setStrokeWidth(this.mBrushSize);
	}

	public void setLastBrushSize(float lastSize){
		this.mLastBrushSize = lastSize;
	}

	public float getLastBrushSize(){
		return this.mLastBrushSize;
	}

	public void setErase(boolean isErase){
		this.isErasing = isErase;

		if (this.isErasing) {
			this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		} else {
			this.mPaint.setXfermode(null);
		}

	}

	public void drawBitmap (Bitmap bm) {
		this.mCanvas.drawBitmap(bm, 0, 0, this.mPaint);
		invalidate();
	}
}
